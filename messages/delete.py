#!/usr/bin/env python3

import boto3

table_name = "Messages"
table = boto3.resource("dynamodb").Table(table_name)

# NOTE: this is highly inefficient by removing item by item - only for testing mode.
# We can delete and recreate the table. However it's taking sooooo looooong for DynamoDb to recreate a table and populate the index.

try:
    #delete items in table
    print(f'Cleaning up all items in {table_name}...')
    response = table.scan()
    for item in response["Items"]:
        table.delete_item(Key={"MessageID": item["MessageID"], "SK": item["SK"]})

    #delete the table if needed 
    # table.delete()
    # print(f'{table_name} table deleted')

except Exception as e:
    print("Error deleting data.")
    print(e)

except ClientError as e:
    print(e)