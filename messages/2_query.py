#!/usr/bin/env python3

import time

import boto3
from boto3.dynamodb.conditions import Key

table_name = "Messages"
table = boto3.resource("dynamodb").Table(table_name)

# NOTE: generating index can take a significant amount of time at 1st time -> avoid deleting & recreating the table!
while True:
    if (
        not table.global_secondary_indexes
        or table.global_secondary_indexes[0]["IndexStatus"] != "ACTIVE"
    ):
        print("Waiting for index to backfill...")
        time.sleep(5)
        table.reload()
    else:
        break

pharID1 = f'phar_91'
userID1 = f'user_61130'
messageID1 = f'{pharID1}#{userID1}'

userID2 = f'user_11345'
messageID2 = f'{pharID1}#{userID2}'

pharID2 = f'phar_12'
messageID3 = f'{pharID2}#{userID2}'

print(f">>> Getting chat channels for specific user / pharmacist <<<")

#get all chat channels for specific user & pharmacist
print(f"\n \t by Phar ID: {pharID1}\n")
resp = table.query(
    IndexName="Message-index",
    KeyConditionExpression=Key("SK").eq(pharID1),
)
for item in resp["Items"]:
    print(item)
print(f"\n")

print(f"\t by User ID: {userID1}\n")
resp = table.query(
    IndexName="Message-index",
    KeyConditionExpression=Key("SK").eq(userID1),
)
for item in resp["Items"]:
    print(item)
print(f"\n")

print(f"\t by User ID: {userID2}\n")
resp = table.query(
    IndexName="Message-index",
    KeyConditionExpression=Key("SK").eq(userID2),
)
for item in resp["Items"]:
    print(item)
print(f"\n")

#1st chat channel 
print(f">>> 1st chat channel: get all messages for {messageID1} << \n")
resp = table.query(
    KeyConditionExpression=Key("MessageID").eq(messageID1)
    & Key("SK").begins_with("MSG"),
)
for item in resp["Items"]:
    print(item)
print(f"\n")

#2nd chat channel
print(f">>> 2nd chat channel: get all messages for {messageID2} << \n")
resp = table.query(
    KeyConditionExpression=Key("MessageID").eq(messageID2)
    & Key("SK").begins_with("MSG"),
)
for item in resp["Items"]:
    print(item)
    
print(f"\n")