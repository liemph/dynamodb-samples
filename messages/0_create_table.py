#!/usr/bin/env python3

import boto3
from botocore.exceptions import ClientError

dynamodb = boto3.resource("dynamodb")
client = dynamodb.meta.client
table_name = "Messages"
gsi_name = "Message-index"
existing_tables = client.list_tables()["TableNames"]

if table_name not in existing_tables:
    print(f"Creating table {table_name}. This can take some time to complete.")
    table = dynamodb.create_table(
        TableName=table_name,
        KeySchema=[
            {"AttributeName": "MessageID", "KeyType": "HASH"},
            {"AttributeName": "SK", "KeyType": "RANGE"},
        ],
        AttributeDefinitions=[
            {"AttributeName": "MessageID", "AttributeType": "S"},
            {"AttributeName": "SK", "AttributeType": "S"},
        ],
        BillingMode="PAY_PER_REQUEST",
    )

    waiter = client.get_waiter("table_exists").wait(TableName=table_name)
    print(table.item_count)
else:
    table = dynamodb.Table(table_name)

# Create Global Secondary Index

try:
    print(f"Creating GSI {gsi_name}")
    resp = client.update_table(
        TableName=table_name,
        AttributeDefinitions=[{"AttributeName": "SK", "AttributeType": "S"}],
        GlobalSecondaryIndexUpdates=[
            {
                "Create": {
                    "IndexName": gsi_name,
                    "KeySchema": [{"AttributeName": "SK", "KeyType": "HASH"}],
                    "Projection": {"ProjectionType": "ALL"},
                }
            }
        ],
    )
    print(f"GSI {gsi_name} added. This can take some time to complete.")
except Exception as e:
    print("Error updating table:")
    print(e)

except ClientError as e:
    print(e)
