#!/usr/bin/env python3

import boto3 
from botocore.exceptions import ClientError

dynamodb = boto3.resource("dynamodb")
client = dynamodb.meta.client
table_name = "Messages"
gsi_name = "Message-index"

table = dynamodb.Table(table_name)

pharID1 = f'phar_91'
userID1 = f'user_61130'
messageID1 = f'{pharID1}#{userID1}'

userID2 = f'user_11345'
messageID2 = f'{pharID1}#{userID2}'

pharID2 = f'phar_12'
messageID3 = f'{pharID2}#{userID2}'


try:
    with table.batch_writer() as batch:
        #1st chat thread
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": messageID1,
                "Timestamp": "2020-12-13T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": pharID1,
                "Timestamp": "2020-12-13T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": userID1,
                "Timestamp": "2020-12-13T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": "STATUS#Active",
                "Timestamp": "2020-12-13T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": "MSG#1607846108",
                "Data": { "from": pharID1, "content": "chào bạn" },
                "Timestamp": "2020-12-13T07:33:32+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": "MSG#1607846112",
                "Data": { "from": pharID1, "content": "mình cần mua sản phẩm gì ạ" },
                "Timestamp": "2020-12-13T07:34:17+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": "MSG#1607846964",
                "Data": { "from": userID1, "content": "Xin chào, mình đang cần mua 1 hộp ABCDEF" },
                "Timestamp": "2020-12-13T07:39:01+00:00"
            }
        ),
        #testing ordering - this is 1st item but inserted late into the table
        batch.put_item(
            Item={
                "MessageID": messageID1,
                "SK": "MSG#1607846008",
                "Data": { "from": userID1, "content": "Xin chào" },
                "Timestamp": "2020-12-13T07:31:01+00:00"
            }
        ),

        #2nd chat thread
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": messageID2,
                "Timestamp": "2020-12-14T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": pharID1,
                "Timestamp": "2020-12-14T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": userID2,
                "Timestamp": "2020-12-14T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": "STATUS#Active",
                "Timestamp": "2020-12-14T07:32:50+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": "MSG#1607934447",
                "Data": { "from": pharID1, "content": "Cửa hàng 24h xin chào" },
                "Timestamp": "2020-12-14T07:33:32+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": "MSG#1607934459",
                "Data": { "from": pharID1, "content": "Quý khách đang cần hỗ trợ gì ạ? Vui lòng gởi hình ảnh" },
                "Timestamp": "2020-12-14T07:34:17+00:00"
            }
        ),
        batch.put_item(
            Item={
                "MessageID": messageID2,
                "SK": "MSG#1607934449",
                "Data": { "from": userID2, "content": "mình đang tìm 2 sản phẩm không biết tên - có hình đính kèm" },
                "Timestamp": "2020-12-14T07:34:01+00:00"
            }
        ),


except ClientError as e:
    print(e)
