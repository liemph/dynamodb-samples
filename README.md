# Purpose
A sample DynamoDB table for messaging

## Preprequisites
* An AWS IAM that has access to DynamoDB APIs. Use aws configure or edit the ~/.aws/credentials file
```
$ aws configure
```
* Python 3.6+


## Steps:

```
#make the files executable
$ chmod 755 *

#create table structure if not yet done
$ 0_create_table.py 

#seed sample data
$ 1_seed_data.py 

#run sample queries
$ 2_query.py
```

## Cleanup table data (and delete table) if needed:
```
$ delete.py
```